\documentclass[12pt]{article}
\usepackage[english]{babel}
\usepackage{natbib}
\usepackage{url}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\graphicspath{{images/}}
\usepackage{parskip}
\usepackage{fancyhdr}
\usepackage{vmargin}
\setmarginsrb{3 cm}{2.5 cm}{3 cm}{2.5 cm}{1 cm}{1.5 cm}{1 cm}{1.5 cm}


\begin{document}

\setcounter{section}{1}

\section{Define von Mises and mixtures of von Mises distributions.}
The von Mises distribution, or the circular normal is defined as:

\[ 
V(\theta | \theta_0, m) = \frac{1}{2\pi I_0(m)}e^{m\: cos(\theta - \theta_0)}
\]

\[
0 \leq \theta < 2 \pi, \; m \geq 0, \; 0 \leq \theta_0 < 2 \pi
\]

where $I_0$ in the normalization factor is the modified zero-order Bessel function of the first kind, and expressed as:

\[
I_0(m) = \frac{1}{2\pi} \int_0^{2\pi}exp \{m\: cos(\theta)\} \; d\theta
\]

or

\[
I_0(m) = \sum_{r=0}^\infty \frac{1}{r!^2}(\frac{m}{2})^{2r}
\]

The mean direction is specified by the $\theta_0$ parameter. The parameter $m$ influences how concentrated the distribution is around the mean direction. Larger values of $m$ result in the distribution being more tightly clustered about the mean direction, and von Mises distribution increasingly resembles the Gaussian with variance 1/$m$.  For $m$ = 0 the distribution collapses in a uniform distribution. \newline

Mixtures of von Mises distributions:

\[
MovM(\theta| \boldsymbol{\theta_0, m}) = \sum_{k=1}^K \pi_kV(\theta | \theta_{0,k},m_k)
\]

where $K$ is the number of mixture’s components and $\pi_k$ the weight of the $k$th component.


\section{A priori, would a mixture of von Mises distributions be more or less adequate than Gaussian mixtures on the real data set of part 1.1? Why?}

A mixture of von Mises distributions is more adequate than Gaussian mixtures, because angles are periodic variables and distributed on the boundary of a circle. To model circular data problems the bivariate von Mises is typically used. Using the Gaussian mixtures would be less adequate because the results would depend on the arbitrary choice of the origin, while von Mises distribution is independent of the origin.

\section{Provide equations for the E-step and M-step of the EM algorithm for mixtures of von Mises distributions. Justify these results with formal computations.}

$1) \; Expectation \: step: \newline$

Given the set $\boldsymbol{\theta}$ = \{$\theta_1, ... , \theta_n$\} of i.i.d. observations and the set of corresponding latent points $\boldsymbol{Z}$ (introduced with a 1-of-K variable $z_i$), we can write the likelihood of the complete data set \{$\boldsymbol{\theta, Z}$\} for the Mixture of von Mises as follows:

\[
p(\boldsymbol{\theta, Z | \pi, \theta_0, m}) = \prod_{i=1}^n \prod_{k=1}^K \pi_k^{Z_{ik}} V (\theta_i|\theta_{0,k}, m_k)^{z_{ik}}
\]

where $z_{ik}$ denotes the $k$th component of $z_i$ and the set of parameters to be estimated through EM is \{$\boldsymbol{\pi,\theta_0,m}$\}, where $\boldsymbol{\pi}$ represents the weights, $\boldsymbol{\theta}$ the mean values, and $\boldsymbol{m}$ the precisions of the mixture’s components. Thus, the expected value of the log likelihood for the complete data set $\mathbb{E}_Z \equiv \mathbb{E}_Z [ln \, p(\boldsymbol{\theta, Z | \pi, \theta_0, m})]$ results to be:

\[
\mathbb{E}_Z = \sum_{i=1}^n \sum_{k=1}^K \gamma_{ik} \{ln \, \pi_k + ln \, V (\theta_i | \theta_{0,k}, m_k)\}
\]

\[
= \sum_{i=1}^n \sum_{k=1}^K \gamma_{ik} \{ln \, \pi_k - ln \, 2\pi I_0(m_k) + m_k \, cos(\theta_i - \theta_{0,k})\}
\]

where $\gamma_{ik} \equiv \mathbb{E}[z_{ik}]$ is the responsibility of component $k$ for data point $\theta_i$, and can be estimated using the parameter values of the previous iteration (randomly initialized for the first iteration) as follows::

\[
\gamma_{ik} = \frac{\pi_k \, V(\theta_i | \theta_{0,k}, m_k)}{\sum_{s=1}^K \pi_s \, V(\theta_i | \theta_{0,s}, m_s)}
\]

$2) \; Maximization \: step: \newline$

The M step maximizes the log likelihood of $\mathbb{E}_Z$ with respect to each of the parameters \{$\boldsymbol{\pi, \theta_0, m}$\}. Computing the derivative of $\mathbb{E}_Z$ with respect to $\pi_k$ by making use of a Lagrange multiplier, we can simply obtain:

\[
\pi = \frac{1}{n} \sum_{i=1}^n \gamma_{ik}
\]

Deriving $\mathbb{E}_Z$ on $\theta_{0,k}$ and setting the result equal to zero, leads to the following equation:

\[
\sum_{i=1}^n \gamma_{ik} m_k \, \sin{(\theta_i - \theta_{0,k})} = 0
\]

which can lead to:

\[
\theta_{0,k} = \arctan \left( \frac{\sum_{i=1}^n \gamma_{ik} \sin{\theta_i} }{ \sum_{i=1}^n \gamma_{ik} \cos{\theta_i} } \right)
\]

Maximization with respect to $m_k$ is not trivial due to the presence of the Bessel function

\[
\frac{\partial \mathbb{E}_Z}{\partial m_k} = \frac{I_1 (m_k)}{I_0 (m_k)} \sum_{i=1}^n \gamma_{ik} + \sum_{i=1}^n \gamma_{ik} \, \cos{(\theta_i - \theta_{0,k})} = 0
\]

This equation can be solved in $m_k$ by introducing a function $A(m_k) = \frac{I_1 (m_k)}{I_0 (m_k)}$ and obtaining:

\[
A(m_k) = \left( \frac{\sum_{i=1}^n \gamma_{ik} \cos{(\theta_i - \theta_{0,k})} }{ \sum_{i=1}^n \gamma_{ik}} \right)
\]

The right side of the equation can be evaluated and then the value of $m_k$ can be computed by inverting the function numerically.

$3) \; Iterate \: procedure: \newline$

Steps 1 and 2 are iterated until convergence (reached when the likelihood does not change too much between two consecutive iterations) or until a given number of iterations is reached.

\section{References}
1. CALDERARA et al. "MIXTURES OF VON MISES DISTRIBUTIONS FOR PEOPLE TRAJECTORY SHAPE ANALYSIS" p. 457, p. 459 eq 1, eq 3, p. 460, p. 468 \newline
2. John Bentley "MODELLING CIRCULAR DATA USING A MIXTURE OF VON MISES AND UNIFORM DISTRIBUTIONS" p. iii, p. 1, p. 4  \newline
3. R. M. Lark et al. "Modelling complex geological circular data with the projected normal distribution and mixtures of von Mises distributions" p.631 \newline
4. Qifeng Jiang "ON FITTING A MIXTURE OF TWO VON MISES DISTRIBUTIONS, WITH APPLICATIONS" p. 1, p. 3 \newline
5. Bishop, "Pattern Recognition and Machine Learning" p. 108, equation 2.179 and p. 693 \newline
6. Parthan Kasarapu "Mixtures of Bivariate von Mises Distributions with Applications to Modelling of Protein Dihedral Angles" p. 1, p. 2


\pagebreak


\end{document}
